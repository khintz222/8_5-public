#pragma once
#include "Table2D.h"
class Table3D : public Table2D
{
	Table2D*table;
public:
	Table3D();
	Table3D(Table2D*, int);

	Table3D& operator=(const Table3D&);
	friend std::ostream& operator<<(std::ostream&, const Table3D&);

	Table2D min();
	Table2D max();

	void sort() override;


	virtual ~Table3D();
};

