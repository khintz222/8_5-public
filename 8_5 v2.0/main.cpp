#include <iostream>
#include "Table1D.h"
#include <time.h>
#include "Table2D.h"
#include "Table3D.h"
using namespace std;



int* randomTable1D( int size)
{
	int* tab = new int[size];
	for (int i = 0; i < size; i++)
	{
		*(tab + i) = rand() % 201 - 100;
	}
	return tab;
}

Table1D* randomTable2D(int size)
{
	Table1D*tab = new Table1D[size];
	for (int i = 0; i < size; i++)
	{
		int sizeI = rand() % 11;
		*(tab + i) = Table1D(randomTable1D(sizeI), sizeI);
	}
	return tab;
}

Table2D* randomTable3D(int size)
{
	Table2D*tab = new Table2D[size];

	for(int i = 0 ; i < size; i++)
	{
		int sizeI = rand() % 11;
		*(tab + i) = Table2D(randomTable2D(sizeI), sizeI);
	}
	return tab;
}

int main(void)
{
	srand(time(NULL));

	Table1D tablica1d;
	Table2D tablica2d;
	Table3D tablica3d;
	tablica1d = Table1D(randomTable1D(10), 10);
	cout << "Tablica 1D\n" << tablica1d << "\nmin\n" << tablica1d.min() << "\nmax\n" << tablica1d.max() <<endl;
	tablica2d = Table2D(randomTable2D(5), 5);
	cout << "Tablica 2D\n" << tablica2d << "\nmin\n" << tablica2d.min() << "\nmax\n" << tablica2d.max() << endl;
	tablica3d = Table3D(randomTable3D(3), 3);
	cout << "Tablica 3D\n" << tablica3d << "\nmin\n" << tablica3d.min() << "\nmax\n" << tablica3d.max() << endl;
	system("pause");
}
