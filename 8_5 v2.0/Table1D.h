#pragma once
#include <ostream>

class Table1D
{
	int* table;
protected:
	int size;
public:
	/**
	 * \brief konstruktor ustawiaj�cy wska�nik na nullptr oraz rozmiar na 0wrzywaany przy two�eniu pustej tablicy 
	 */
	Table1D();
	/**
	 * \brief konstruktor wype�na tablic� podanymi danymi je�eli tylko s� r�ne od nullptr oraz rozmiar wi�kszy od 0
	 */
	Table1D(int*, int);

	/**
	 * \brief konstruktor umozliwiaj�cy wtwo�enie innych tablic ustawia tylko rozmiar
	 */
	Table1D(int);

	/**
	 * \brief operator przypisana przypisuje jedn� raablic� do drugiej sprawdzaj�c czy druga z tablic nie jest pusta je�eli taak to opr�nia t� pierwsz� 
	 * \return zwraca referencj� do this :) 
	 */
	Table1D& operator=(const Table1D&);

	/**
	 * \brief opereator << ypisuje na ekran komunikat EMPTY TABLE, lub wypisuje dane zawarte w tablicy
	 * \return referencja do ostream
	 */
	friend std::ostream& operator<<(std::ostream&, const Table1D&);


	/**
	 * \brief funkcja szuka najmniejszej warto�ci tablicy
	 * !!! Funkcja �le dzia�a je�eli w jednej lini do pustej tablicy przypisujemy taaablic� zape�nion�a nast�pinie szukamy min !!!
	 * \return najmnijsz� warto�� tabicy lub ww przypadku tablcy pustej zwaraca 0 !! Uawaga 0 mo�e by� r�nie� najmniejszym elementem tablicy !!
	 */
	int min();

	/**
	 * \brief funkcja szuka najwi�kszej warto�ci tablicy
	 * !!! Funkcja �le dzia�a je�eli w jednej lini do pustej tablicy przypisujemy taaablic� zape�nion�a nast�pinie szukamy max !!!
	 * \return najwi�ksz� warto�� tabicy lub ww przypadku tablcy pustej zwaraca 0 !! Uawaga 0 mo�e by� r�nie� najwi�kszym elementem tablicy !!
	 */
	int max();

	virtual void sort();

	/**
	 * \brief destruktor usuwa tawskaznik do table 
	 */
	virtual ~Table1D();
};
