#include "Table2D.h"
#include <iostream>


Table2D::Table2D(): Table1D(0), table(nullptr){}

Table2D::Table2D(Table1D* tab, int size): Table1D(size)
{
	if (size < 1 || tab == nullptr)
	{
		this->size = 0;
		this->table = nullptr;
	}
	else
	{
		this->table = new Table1D[this->size];

		for (int i = 0; i < this->size; i++)
		{
			*(this->table + i) = *(tab + i);
		}
	}
}

Table2D::Table2D(int size) :Table1D(size) { this->table = nullptr; }

Table2D& Table2D::operator=(const Table2D& other)
{
	if (other.table == nullptr)
	{
		if (this->table != nullptr)
		{
			delete[]this->table;
			this->table = nullptr;
			this->size = 0;
		}
	}
	else
	{
		if (this->size != other.size)
		{
			delete[]this->table;
			this->table = new Table1D[this->size = other.size];
		}

		for (int i = 0; i < this->size; i++)
		{
			*(this->table + i) = *(other.table + i);
		}
	}
	return *this;
}

Table1D Table2D::min()
{
	if (this->table == nullptr || this->size == 0)return Table1D();
	int *tab = new int[this->size];
	
	for (int i = 0; i < this->size; i++)
	{
		*(tab + i) = (this->table + i)->min();
		
	}
	
	
	return Table1D(tab, this->size);
}

Table1D Table2D::max()
{
	if (this->table == nullptr || this->size == 0)return Table1D();
	int *tab = new int[this->size];

	for (int i = 0; i < this->size; i++)
	{
		*(tab + i) = (this->table + i)->max();

	}


	return Table1D(tab, this->size);
}

void Table2D::sort()
{
	if (this->size == 0 || this->table == nullptr)return;

	for (int i=0 ; i < this->size; i++)
	{
		(this->table + i)->sort();
	}
}

Table2D::~Table2D()
{
	delete[]this->table;
}

std::ostream& operator<<(std::ostream& out, Table2D& data)
{
	if (data.size == 0 || data.table == nullptr) { out << "[ EMPTY TABLE ]"; }
	else
	{
		out << "\t[" << std::endl;
		for (int i = 0; i < data.size; i++)
		{
			out << *(data.table + i) << std::endl;
		}
		out << "\t]";
	}
	return out;
}
