#include "Table1D.h"
#include <iostream>


Table1D::Table1D()
{
	this->table = nullptr;
	this->size = 0;
}

Table1D::Table1D(int* tab, int size): size(size)
{
	if (size < 1 || tab == nullptr)
	{
		this->table = nullptr;
		this->size = 0;
		return;
	}
	this->table = new int[this->size];

	for (int i = 0; i < this->size; i++)
	{
		*(this->table + i) = *(tab + i);
	}
}

Table1D::Table1D(int size): table(nullptr), size(size)
{
}

Table1D& Table1D::operator=(const Table1D& other)
{
	if (other.table == nullptr)
	{
		if (this->table != nullptr)
		{
			delete[]this->table;
			this->table = nullptr;
			this->size = 0;
		}
	}
	else
	{
		if (this->size != other.size)
		{
			delete[]this->table;
			this->table = new int[this->size = other.size];
		}

		for (int i = 0; i < this->size; i++)
		{
			*(this->table + i) = *(other.table + i);
		}
	}
	return *this;
}

int Table1D::min()
{
	if (this->table == nullptr || this->size == 0)
		return 0;
	int min = *(this->table);
	for (int i = 1; i < this->size; i++)
	{
		if (*(this->table + i) < min)min = *(this->table + i);
	}
	return min;
}

int Table1D::max()
{
	if (this->table == nullptr || this->size == 0)
		return 0;
	int max = *(this->table);
	for (int i = 1; i < this->size; i++)
	{
		if (*(this->table + i) > max)max = *(this->table + i);
	}
	return max;
}

void Table1D::sort()
{
	//sortowanie przez wstawianie 
	if (this->table == nullptr || this->size == 0)return;

	int w = 0; // wartownik
	int i; //iterator 
	while (w < this->size - 1) // p�tla g��wna przechodzi przez elementy od 0 do n-1
	{
		i = w + 1;
		while (i < this->size) //p�tla dodatkowa przechoddzi przez elementy od w do n
		{
			if (*(this->table + w) > *(this->table + i))
				// funkcja warunkowa kt�raw przypadku gdy element w jest wi�kszy od elementu i zamienia je miejscami
			{
				int bufor = *(this->table + w);
				*(this->table + w) = *(this->table + i);
				*(this->table + i) = bufor;
			}
			i++;
		}
		w++;
	}
}

Table1D::~Table1D()
{
	delete[]this->table;
}

std::ostream& operator<<(std::ostream& out, const Table1D& data)
{
	if (data.table == nullptr || data.size == 0)return out << "[ EMPTY TABLE ]";
	out << "[\t";
	for (int i = 0; i < data.size; i++)
	{
		out << *(data.table + i) << (i < data.size - 1 ? ",\t" : "\t");
	}
	out << "]";
	return out;
}
