#include "Table3D.h"



Table3D::Table3D():Table2D()
{
	this->table = nullptr;
}

Table3D::Table3D(Table2D*tab, int size):Table2D(size)
{
	if (size == 0 || tab == nullptr)
	{
		this->table = nullptr;
		this->size = 0;
	}else
	{
		this->table = new Table2D[this->size];

		for (int i = 0; i < this->size; i++)
		{
			*(this->table + i) = *(tab + i);
		}
	}
}

Table3D& Table3D::operator=(const Table3D& other)
{
	if (other.table == nullptr)
	{
		if (this->table != nullptr)
		{
			delete[]this->table;
			this->table = nullptr;
			this->size = 0;
		}
	}
	else
	{
		if (this->size != other.size)
		{
			delete[]this->table;
			this->table = new Table2D[this->size = other.size];
		}

		for (int i = 0; i < this->size; i++)
		{
			*(this->table + i) = *(other.table + i);
		}
	}
	return *this;
}

Table2D Table3D::min()
{
	if (this->table == nullptr || this->size == 0)return Table2D(); // je�eli tablica jest pusta zwr�� pust� talic� dwu wymiarow�
	Table1D *tab = new Table1D[this->size]; // utw� tablic� tablic jednowymiarowych o wielko�ci tablicy 3 d (z kazdego wymiaruu b�dzie wybierane minimum) 

	for(int i=0;i<this->size;i++)
	{
		*(tab+i) = (this->table+i)->min(); // do tablicy 1D przypisz ynik dzia�ania funkcji min dla tabicy 2D  
	}

	return Table2D(tab, this->size);
}


Table2D Table3D::max()
{
	if (this->table == nullptr || this->size == 0)return Table2D();
	Table1D *tab = new Table1D[this->size];

	for (int i = 0; i < this->size; i++)
	{
		*(tab+i) = (this->table+i)->max();
	}

	return Table2D(tab, this->size);
}

void Table3D::sort()
{
	if (this->table == nullptr || this->size == 0)return;
	for(int i=0;i<this->size;i++)
	{
		(this->table)->sort();
	}
}

Table3D::~Table3D()
{
	delete[]this->table;
}

std::ostream& operator<<(std::ostream& out , const Table3D& data)
{
	if (data.table == nullptr || data.size == 0) return out << "[ EMPTY TABLE ]";
	out << "\t\t[\n";
	for (int i=0;i<data.size;i++)
	{
		out << *(data.table + i) << std::endl;
	}
	return out << "\t\t]";
}
