#pragma once
#include "Table1D.h"

class Table2D : public Table1D
{
	Table1D* table;
public:
	/**
	 * \brief konstruktor domniemany
	 */
	Table2D();

	/**
	 * \brief kostruktor g��wny 
	 */
	Table2D(Table1D*, int);

	Table2D(int);

	Table2D& operator=(const Table2D&);
	friend std::ostream& operator<<(std::ostream&, Table2D&);

	Table1D min();
	Table1D max();

	void sort() override;

	virtual ~Table2D();
};
